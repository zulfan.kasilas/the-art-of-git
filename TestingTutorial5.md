## API Call

GET /mahasiswa
-------------
* Response
````
[
    {
        "npm": "String",
        "nama": "String",
        "email": "String",
        "ipk": "String",
        "noTelp": "String",
        "logAsdos": [],
    }
]
````

POST /mahasiswa
----------
* Request Body
```
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String"
}
```
* Response
```
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String",
    "logAsdos": [],
}
```

GET /mahasiswa/{npm}
-------------
* Response
````
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String",
    "logAsdos": []
}
````

PUT /mahasiswa/{npm}
----------
* Request Body
```
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String"
}
```
* Response
```
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String",
    "logAsdos": []
}
```

DELETE /mahasiswa/{npm}
----------

* Response
```
```

GET /matakuliah
----------
* Response
```
[
    {
        "kodeMatkul": "String",
        "nama": "String",
        "prodi": "String",
        "asdoss": Mahasiswa[]
    }
]
```

POST /mata-kuliah
----------
* Request Body
```
{
    "kodeMatkul" : "String",
    "nama" : "String",
    "prodi" : "String"
}
```
* Response
```
{
    "kodeMatkul": "String",
    "nama": "String",
    "prodi": "String",
    "asdoss": Mahasiswa[]
}
```

GET /mata-kuliah/{kodeMatkul}
----------
* Response
```
{
    "kodeMatkul": "String",
    "nama": "String",
    "prodi": "String",
    "asdoss": Mahasiswa[]
}
```

PUT /mata-kuliah/{kodeMatkul}
----------
* Request Body
```
{
    "kodeMatkul" : "String",
    "nama" : "String",
    "prodi" : "String"
}
```
* Response
```
{
    "kodeMatkul": "String",
    "nama": "String",
    "prodi": "String",
    "asdoss": Mahasiswa[]
}
```

DELETE /mata-kuliah/{kodeMatkul}
----------
* Response
```
```

POST /log/{npm}
----------
* Request Body
```
{
    "start" : "dd-MM-YYYY HH:MM",
    "end" : "dd-MM-YYYY HH:MM",
    "desc" : "string"
}
```
* Response
```
{
    "logId": int,
    "start" : "dd-MM-YYYY HH:MM",
    "end" : "dd-MM-YYYY HH:MM",
    "desc" : "string",
    "duration": double
}
```

POST /log/daftar/{npm}/{kodeMatkul}
----------
* Request Body
```
```

* Response
```
{
    "npm": "String",
    "nama": "String",
    "email": "String",
    "ipk": "String",
    "noTelp": "String",
    "logAsdos": Log[]
}
```

GET /log/logmahasiswa/{npm}
----------
* Response
```
[
    {
        "logId": int,
        "start" : "dd-MM-YYYY HH:MM",
        "end" : "dd-MM-YYYY HH:MM",
        "desc" : "string",
        "duration": double
    },
]
```

GET /log/summary/{npm}/{bulan}
----------
* Response
```
{
    "bulan": "String",
    "lamaKerja": double,
    "pembayaran": double
}
```

GET /log/summary/{npm}
----------
* Response
```
[
    {
        "bulan": "String",
        "lamaKerja": double,
        "pembayaran": double
    },
]
```

GET /log/{id}
----------
* Response
```
{
    "logId": int,
    "start" : "dd-MM-YYYY HH:MM",
    "end" : "dd-MM-YYYY HH:MM",
    "desc" : "string",
    "duration": double
}
```

PUT /log/{id}
----------
* Request Body
```
{
    "start" : "dd-MM-YYYY HH:MM",
    "end" : "dd-MM-YYYY HH:MM",
    "desc" : "String",
}
```

* Response
```
{
    "logId": int,
    "start" : "dd-MM-YYYY HH:MM",
    "end" : "dd-MM-YYYY HH:MM",
    "desc" : "string",
    "duration": double
}
```

DELETE /log/{id}
----------
* Response
```
```
